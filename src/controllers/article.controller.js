import Joi from 'joi';
import {
  createArticle,
  removeArticle,
  updateArticle,
  getArticles,
  getArticle
} from '../services/article.js';


// Define the schema for the article
const articleSchemaCreate = Joi.object({
  title: Joi.string().min(3).max(255).required(),
  price: Joi.number().required()
});

const articleSchemaUpdate = Joi.object({
  title: Joi.string().min(3).max(255),
  price: Joi.number()
});

// Holen eines bestimmten Artikels
export const get = async (req, res, next) => {
  try {
    const article = await getArticle(req.params.id);
    if (!article) {
      return res.status(404).send({ error: 'Artikel nicht gefunden' }); // Artikel nicht gefunden
    }
    res.status(200).send(article);
  } catch (err) {
    console.error(err);
    res.status(500).send({ error: 'Interner Serverfehler' }); // Allgemeines Fehlermanagement
    next();
  }
};

// Auflisten aller Artikel
export const list = async (req, res, next) => {
  try {
    const articles = await getArticles();
    res.status(200).send(articles);
  } catch (err) {
    console.error(err);
    res.status(500).send({ error: 'Interner Serverfehler' });
    next();
  }
};

// Erstellen eines neuen Artikels
export const create = async (req, res, next) => {
  const { error } = articleSchemaCreate.validate(req.body);
  if (error) {
    return res.status(400).send({ error: `Fehler beim Erstellen des Artikels: ${error.details[0].message}` });
  }

  try {
    const article = await createArticle(req.body);
    res.status(201).send(article); // 201 für erfolgreich erstellte Inhalte
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: 'Fehler beim Erstellen des Artikels' }); // 400 für Anfragefehler
    next();
  }
};

// Aktualisieren eines vorhandenen Artikels
export const update = async (req, res, next) => {
  const { error } = articleSchemaUpdate.validate(req.body);
  if (error) {
    return res.status(400).send({ error: `Fehler beim Aktualisieren des Artikels: ${error.details[0].message}` });
  }

  try {
    const result = await updateArticle(req.params.id, req.body);
    if (!result) {
      return res.status(404).send({ error: 'Artikel nicht gefunden' });
    }
    res.status(200).send(result);
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: 'Fehler beim Aktualisieren des Artikels' });
    next();
  }
};

// Entfernen eines Artikels
export const remove = async (req, res, next) => {
  try {
    const result = await removeArticle(req.params.id);
    if (!result) {
      return res.status(404).send({ error: 'Artikel nicht gefunden' });
    }
    res.status(200).send({ message: 'Artikel erfolgreich entfernt' });
  } catch (err) {
    console.error(err);
    res.status(500).send({ error: 'Interner Serverfehler' });
    next();
  }
};
