import mongoose from 'mongoose';
const { Schema } = mongoose;

// Definition des Schemas für Produkte
export const productSchema = new Schema(
    {
        title: {
            type: String,
            required: true // Titel ist erforderlich
        },
        description: {
            type: String
        },
        active: {
            type: Boolean, // Logischer Wert für aktive Produkte
            default: true // Standardmässig ist das Produkt aktiv
        },
        price: {
            type: Number,
            required: true // Preis ist erforderlich
        },
        image: String, // Bild-URL oder Pfad
        // reviews: [
        //     {
        //         // Schema für Produktbewertungen
        //         title: {
        //             type: String,
        //             required: true // Bewertungstitel ist erforderlich
        //         },
        //         description: {
        //             type: String,
        //             required: true // Bewertungsbeschreibung ist erforderlich
        //         },
        //         rating: {
        //             type: Number,
        //             required: true, // Bewertung ist erforderlich
        //             min: 1, // Mindestbewertung
        //             max: 5 // Höchstbewertung
        //         }
        //     }
        // ]
    },
    { timestamps: true } // Automatische Verwaltung von Erstellungs-/Änderungszeiten
);

// Modell für die `Product` Collection exportieren
export default mongoose.model('Product', productSchema, 'product');
