import mongoose from 'mongoose';
import Article from '../models/product.js';

export async function getArticles() {
  try {
    return await Article.find();
  } catch (error) {
    console.error('Error fetching articles:', error);
    throw new Error('Could not fetch articles');
  }
}

export async function getArticle(id) {
  try {
    return await Article.findById(id);
  } catch (error) {
    console.error('Error fetching article:', error);
    throw new Error('Could not fetch article');
  }
}

export async function createArticle(newArticle) {
  try {
    return await Article.create(newArticle);
  } catch (error) {
    console.error('Error creating article:', error);
    throw new Error('Could not create article');
  }
}

export async function updateArticle(id, updatedArticle) {
  try {
    return await Article.findOneAndUpdate(
        { _id: mongoose.Types.ObjectId(id) },
        updatedArticle,
        { new: true }
    );
  } catch (error) {
    console.error('Error updating article:', error);
    throw new Error('Could not update article');
  }
}

export async function removeArticle(id) {
  try {
    return await Article.findByIdAndDelete(id);
  } catch (error) {
    console.error('Error removing article:', error);
    throw new Error('Could not remove article');
  }
}
