import express from 'express'
import productRoutes from './routes/product.routes.js'
import dotenv from 'dotenv'
import connectDb from './utils/db.js'

dotenv.config();

connectDb();

const app = express()
const port = process.env.PORT;

app.use(express.json());

app.use('/product', productRoutes);

app.listen(port, () => {
  console.log(`Backend app listening at http://localhost:${port}`)
});